import java.io.*;
import java.net.*;
import java.security.MessageDigest;
import java.util.*;
import java.util.Base64.Decoder;
import java.util.Base64.Encoder;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.text.*;

import java.awt.*;
import java.awt.event.*;

public class SimpleChatClientA {

	JLabel yourName;
	JTextPane incoming;
	JTextField outgoing;
	JTextField nick;
	BufferedReader reader;
	PrintWriter writer;
	Socket sock;

	public static void main(String[] args) {
		SimpleChatClientA client = new SimpleChatClientA();
		client.go();
	}

	public void go() {
		JFrame frame = new JFrame("Chat Client");
		JPanel mainPanel = new JPanel();

		incoming = new JTextPane();                
		incoming.setBorder(new EmptyBorder(new Insets(10, 10, 10, 10)));
		incoming.setMargin(new Insets(5, 5, 5, 5));
		incoming.setPreferredSize(new Dimension(500,300));
		incoming.setEditable(false);

		JScrollPane scrollBar = new JScrollPane(incoming);
		scrollBar.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollBar.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

		yourName = new JLabel();
		outgoing = new JTextField(20);
		nick = new JTextField(10);
		yourName.setText("Your Name: ");

		JButton sendButton = new JButton("Send");
		sendButton.addActionListener(new SendButtonListener());

		mainPanel.add(scrollBar);
		mainPanel.add(yourName);
		mainPanel.add(nick);
		mainPanel.add(outgoing);
		mainPanel.add(sendButton);

		setUpNetworking();
		Thread readerThread = new Thread(new IncomingReader());
		readerThread.start();

		frame.getContentPane().add(BorderLayout.CENTER, mainPanel);
		frame.setSize(600,400);
		frame.setVisible(true);
	}

	private void setUpNetworking() {  
		try {
			sock = new Socket("127.0.0.1", 5000);
			InputStreamReader streamReader = new InputStreamReader(sock.getInputStream());
			reader = new BufferedReader(streamReader);

			writer = new PrintWriter(sock.getOutputStream());

			System.out.println("Verbindung hergestellt!");
		} catch(IOException ex) {
			ex.printStackTrace();
		}
	}   

	public class SendButtonListener implements ActionListener {
		public void actionPerformed(ActionEvent ev) {
			try {
				writer.println(encrypt("sammy", nick.getText()+": "+outgoing.getText()));
				writer.flush();

			} catch(Exception ex) {
				ex.printStackTrace();
			}
			outgoing.setText("");
			outgoing.requestFocus();
		}
	}

	public class IncomingReader implements Runnable {
		public void run() {
			String message;
			String farbe=null;
			try {
				while ((message = reader.readLine()) != null) {
					farbe="#"+message.substring(0,6);
					message=decrypt("sammy", message.substring(6));
					System.out.println(message);
					appendToPane(incoming, message + "\n",hex2Rgb(farbe));
				}
			} catch(Exception ex) {ex.printStackTrace();}
		} 
	}  

	public static Color hex2Rgb(String farbString) {
		return new Color(
				Integer.valueOf( farbString.substring( 1, 3 ), 16 ),
				Integer.valueOf( farbString.substring( 3, 5 ), 16 ),
				Integer.valueOf( farbString.substring( 5, 7 ), 16 ) );
	}

	private void appendToPane(JTextPane tp, String msg, Color c) throws BadLocationException{
		StyleContext sc = StyleContext.getDefaultStyleContext();
		AttributeSet aset = sc.addAttribute(SimpleAttributeSet.EMPTY, StyleConstants.Foreground, c);

		aset = sc.addAttribute(aset, StyleConstants.FontFamily, "Lucida Console");
		aset = sc.addAttribute(aset, StyleConstants.Alignment, StyleConstants.ALIGN_JUSTIFIED);

		int len = tp.getDocument().getLength();
		Document doc=tp.getDocument();
		tp.setCaretPosition(len);
		tp.setCharacterAttributes(aset, false);
		doc.insertString(len, msg, aset);
		tp.invalidate();
	}

	String encrypt (String pw, String text) throws Exception
	{
		String keyStr = pw;
		byte[] key = keyStr.getBytes("UTF-8");

		MessageDigest sha = MessageDigest.getInstance("SHA-256");
		key = sha.digest(key);
		key = Arrays.copyOf(key, 16);

		SecretKeySpec secretKeySpec = new SecretKeySpec(key, "AES");

		Cipher cipher = Cipher.getInstance("AES");
		cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec);
		byte[] encrypted = cipher.doFinal(text.getBytes());

		Encoder myEncoder = Base64.getEncoder();
		String codedcrypted = myEncoder.encodeToString(encrypted);

		return codedcrypted;
	}

	String decrypt (String pw, String textToEnc) throws Exception
	{
		String keyStr = pw;
		byte[] key = keyStr.getBytes("UTF-8");

		MessageDigest sha = MessageDigest.getInstance("SHA-256");
		key = sha.digest(key);
		key = Arrays.copyOf(key, 16);

		SecretKeySpec secretKeySpec = new SecretKeySpec(key, "AES");

		Decoder myDecoder = Base64.getDecoder();
		byte[] crypted = myDecoder.decode(textToEnc);

		Cipher cipher2 = Cipher.getInstance("AES");
		cipher2.init(Cipher.DECRYPT_MODE, secretKeySpec);
		byte[] decrypted = cipher2.doFinal(crypted);
		String erg = new String(decrypted);

		return  erg;
	}

}

